const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');
const AdminsController = require('../controllers/admins');




router.post("/signup", AdminsController.admins_signup_admin);

router.post("/login", AdminsController.admins_login_admin);

router.post("/deleteAdmin", checkAuth, AdminsController.admins_delete_admin);

router.post("/getAdmin", checkAuth, AdminsController.admins_get_admin);


module.exports = router;