const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');
const MunicipalitiesController = require('../controllers/municipalities');


router.post('/getMunicipality', checkAuth, MunicipalitiesController.municipalities_get_municipality);

router.post('/createMunicipality', checkAuth, MunicipalitiesController.municipalities_create_municipality);

router.post('/deleteMunicipality', checkAuth, MunicipalitiesController.municipalities_delete_municipality);

router.post('/updateMunicipality', checkAuth, MunicipalitiesController.municipalities_update_municipality);




module.exports = router;