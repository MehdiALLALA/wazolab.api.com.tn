const express = require('express');
const router = express.Router();
const multer = require('multer');
const checkAuth = require('../middleware/check-auth');
const PostsController = require('../controllers/posts');


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './uploads/');
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + file.originalname);
    }
  });

const fileFilter = (req, file, cb) => {
   if((file.mimetype === 'image/png' || file.mimetype === 'image/jpeg')){
       cb(null,true);
   }
   else{
       cb(null,false);
   }
};

const upload = multer({ 
    storage : storage,
    fileFilter : fileFilter
});


router.post('/getPost', checkAuth, PostsController.posts_get_post);

router.post('/createPost', checkAuth, upload.single('post_image'),PostsController.posts_create_post);

router.post('/deletePost', checkAuth, PostsController.posts_delete_post);

router.post('/getPostByMunicipality', checkAuth, PostsController.posts_get_post_bymunicipality);






module.exports = router;