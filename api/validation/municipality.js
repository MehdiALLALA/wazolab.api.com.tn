const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateMunicipalityInput(data) {
  let errors = {};

  data.city = !isEmpty(data.city) ? data.city : '';
  data.governorate = !isEmpty(data.governorate) ? data.governorate : '';
  data.cityLatitude = !isEmpty(data.cityLatitude) ? data.cityLatitude : '';
  data.cityLongitude = !isEmpty(data.cityLongitude) ? data.cityLongitude : '';

  if (Validator.isEmpty(data.governorate)) {
    errors.governorate = 'Governorate field is required';
  }

  if (Validator.isEmpty(data.city)) {
    errors.city = 'City field is required';
  }
  if (!Validator.isNumeric(data.cityLatitude) ) {
    errors.cityLatitude = 'City Latitude must be a Numeric value';
  }

  if (!Validator.isNumeric(data.cityLongitude)) {
    errors.cityLongitude = 'City Longitude must be a Numeric value';
  }
  
  if (Validator.isEmpty(data.cityLatitude)) {
    errors.cityLatitude = 'City Latitude field is required';
  }

  if (Validator.isEmpty(data.cityLongitude)) {
    errors.cityLongitude = 'City Longitude field is required';
  }



  

  return {
    errors,
    isValid: isEmpty(errors)
  };
};