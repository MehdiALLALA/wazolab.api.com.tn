const mongoose = require('mongoose');
const User = require('../models/user');
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const validateLoginInput = require('../validation/login');

exports.users_signup_user = (req, res, next) => {
    User.find({ email: req.body.email })
    .exec()
    .then( user => {
      if(user.length >= 1){
        res.status(409).json({
          message: "Email exists"
        });
      }
      else {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err
            });
          } else {
            const user = new User({
              _id: new mongoose.Types.ObjectId(),
              email: req.body.email,
              password: hash,
              municipality: req.body.municipality
            });
            user
              .save()
              .then(result => {
                console.log(result);
                res.status(201).json({
                  message: "User created"
                });
              })
              .catch(err => {
                console.log(err);
                res.status(500).json({
                  error: err
                });
              });
          }
         });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
    
              
  };

  exports.users_login_user = (req, res, next) => {


    const { errors, isValid } = validateLoginInput(req.body);
    
    if (!isValid) {
      return res.status(400).json(errors);
    }

    User.find({ email: req.body.email })
      .exec()
      .then(user => {
        if (user.length < 1) {
          errors.email = 'User not found';
          return res.status(401).json(errors);
        }
        bcrypt.compare(req.body.password, user[0].password).then(isMatch => {
          if (!isMatch) {
            errors.password = 'Password incorrect';
            return res.status(401).json(errors);
          }
          else {
            const token = jwt.sign(
              {
                email: user[0].email,
                userId: user[0]._id,
                municipalityId: user[0].municipality
              },
              config.get('jwtSecret'),
              {
                  expiresIn: "72h"
              }
            );
            return res.status(200).json({
              message: "Auth successful",
              token: "BEARER "+token
            });
          }
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  };

exports.users_delete_user = (req, res, next) => {
    User.remove({ _id: req.body.userId })
      .exec()
      .then(result => {
        res.status(200).json({
          message: "User deleted"
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  };

  exports.users_get_user = (req, res, next) => {
    const id = req.body.userId;
    if(!id){
      User.find()
        .select('_id email password municipality')
        .populate('municipality')
        .exec()
        .then(docs => {
            res.status(200).json(docs);
        })
        .catch( err => {
            res.status(500).json({
                error : err
            });
        });
    }
    else{
        User.find({_id : id})
        .select('_id email password municipality')
        .populate('municipality')
        .exec()
        .then( doc => {
            if(doc) {
                res.status(200).json(doc);
            }
            else {
                res.status(404).json({
                   message : "No valid entry found for the given ID" 
                }) 
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        });
    }
};