const mongoose = require("mongoose");
const Post = require("../models/post");
const User = require("../models/user");
const fs = require ('fs');


exports.posts_get_post = (req, res, next) => {
    const id = req.body.userId;
    let dates = []
    if(req.body.startDate){
        dates.push({"date":{"$gte": new Date(req.body.startDate).toISOString().replace(/T/, ' ')
        .replace(/\..+/, '')}});
    }
    if(req.body.endDate){
        dates.push({"date":{"$lt": new Date(req.body.endDate).toISOString().replace(/T/, ' ')
        .replace(/\..+/, '')}});
    }
    if(!id){
        if(dates.length == 0){
        Post.find()
        .select('_id date rating post_image altitude longitude status')
        .populate('user','email')
        .exec()
        .then(docs => {
            if(docs.length > 0){
                docs.forEach(function(doc){
                    doc.post_image = doc.post_image.replace('\\','/')
                })
            }
            res.status(200).json(docs);
        })
        .catch( err => {
            res.status(500).json({
                error : err
            });
        });
    
    }
    else {
        Post.find()
        .select('_id date rating post_image altitude longitude status')
        .populate('user','email')
        .and(dates)
        .exec()
        .then(docs => {
            if(docs.length > 0){
                docs.forEach(function(doc){
                    doc.post_image = doc.post_image.replace('\\','/')
                })
            }
            res.status(200).json(docs);
        })
        .catch( err => {
            res.status(500).json({
                error : err
            });
        });
    }
    }
    else{
        if(dates.length == 0){
        Post.find({user : id})
       .select('_id date rating post_image altitude longitude status user')
       .populate('user','email')
        .exec()
        .then( docs => {
            if(docs.length > 0) {
                
                docs.forEach(function(doc){
                    doc.post_image = doc.post_image.replace('\\','/');
                })
            }
            res.status(200).json(docs);
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        });
    }
    else {
        Post.find({user : id})
        .select('_id date rating post_image altitude longitude status user')
        .populate('user','email')
        .and(dates)
         .exec()
         .then( docs => {
             if(docs.length > 0) {
                 
                 docs.forEach(function(doc){
                     doc.post_image = doc.post_image.replace('\\','/');
                 })
             }
             res.status(200).json(docs);
         })
         .catch(err => {
             res.status(500).json({
                 error: err
             })
         });
    }
    }
};

exports.posts_create_post = (req, res, next) => {
    if(req.file){
        const file_location = req.file.path;
        if(req.body.rating && req.body.altitude && req.body.longitude && req.body.userId && req.body.municipalityId){
            const current_date = new Date(Date.now()).toISOString()
            .replace(/T/, ' ')
            .replace(/\..+/, '');
            console.log(current_date);
            const post = new Post({
                _id : new mongoose.Types.ObjectId(),
                date : current_date,
                rating : req.body.rating,
                post_image : req.file.path,
                altitude : req.body.altitude,
                longitude : req.body.longitude,
                user : req.body.userId,
                municipality : req.body.municipalityId
            });
            post
            .save()
            .then(result => {
                console.log(result);
                res.status(201).json({
                  message: "Created post successfully",
                  createdPost: post
                });
              })
              .catch(err => {
                console.log(err);
                res.status(500).json({
                  error: err
                });
            });
        }
        else {
            const loc = process.cwd();
            fs.unlink(loc + '/' + req.file['post_image'].path, function (err) {
                if (err) throw err;
            });
            res.status(400).json({
                error: "missing rating and/or altitude and/or longitude"
            })

        }

    }
    else {
        res.status(400).json({
            error: "post_image is required"
        })
    }
        
};


exports.posts_delete_post = (req, res, next) => {
    const id = req.body.postId;
    let postImg = null;

    if(id){
        Post.findById(id)
        .exec()
        .then(rs => {
            postImg = rs.post_image;
        })
        .catch(e => {
            console.log(e);
        });
    Post.deleteOne({ _id: id})
    .exec()
    .then( result => {
        const loc = process.cwd();
            fs.unlink(loc+'/'+postImg, function (err) {
                if (err) throw err;
                console.log('File deleted!');
            }); 
        res.status(200).json({
            message : "post deleted sucessfully"
        });
    })
    .catch( err => {
        console.log(err);
        res.status(500).json({
            error : err
        });
    });
}
    else{
    res.status(400).json({
        error : "missing postId"
    })
}
};

exports.posts_get_post_bymunicipality = (req, res, next) => {
        
        const id = req.body.municipalityId;
        let redMarker = 0;
        let yallowMarker = 0;
        let greenMarker = 0;
        Post.find({ municipality : id})
        .select('_id date rating post_image altitude longitude status user municipality')
        .exec()
        .then( docs => {
            docs.forEach(function(doc){
                if(doc.rating === "Good")
                    greenMarker++;
                if(doc.rating === "bad")
                    redMarker++;
                if(doc.rating === "not-so-bad")
                    yallowMarker++;
                
            })
            res.status(200).json({
                greenMarker,
                yallowMarker,
                redMarker,
                Total: greenMarker+yallowMarker+redMarker
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        });
    
};
    
 



