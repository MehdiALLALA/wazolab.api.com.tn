const mongoose = require('mongoose');
const Admin = require('../models/admin');
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");

exports.admins_signup_admin = (req, res, next) => {
    Admin.find({ email: req.body.email })
    .exec()
    .then( admin => {
      if(admin.length >= 1){
        res.status(409).json({
          message: "Email exists"
        });
      }
      else {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err
            });
          } else {
            const admin = new Admin({
              _id: new mongoose.Types.ObjectId(),
              email: req.body.email,
              password: hash,
              municipality: req.body.municipalityId,
              super_admin: req.body.superAdmin
            });
            admin
              .save()
              .then(result => {
                console.log(result);
                res.status(201).json({
                  message: "Admin created"
                });
              })
              .catch(err => {
                console.log(err);
                res.status(500).json({
                  error: err
                });
              });
          }
         });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
    
              
  };

  exports.admins_login_admin = (req, res, next) => {
    Admin.find({ email: req.body.email })
      .exec()
      .then(admin => {
        if (admin.length < 1) {
          return res.status(401).json({
            message: "Auth failed"
          });
        }
        bcrypt.compare(req.body.password, admin[0].password, (err, result) => {
          if (err) {
            return res.status(401).json({
              message: "Auth failed"
            });
          }
          if (result) {
            const token = jwt.sign(
              {
                email: admin[0].email,
                userId: admin[0]._id,
                municipalityId: admin[0].municipality,
                super_admin: admin[0].super_admin
              },
              config.get('jwtSecret'),
              {
                  expiresIn: "3h"
              }
            );
            return res.status(200).json({
              message: "Auth successful",
              token: token
            });
          }
          res.status(401).json({
            message: "Auth failed"
          });
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  };

exports.admins_delete_admin = (req, res, next) => {
    Admin.remove({ _id: req.body.adminId })
      .exec()
      .then(result => {
        res.status(200).json({
          message: "Admin deleted"
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  };
  exports.admins_get_admin = (req, res, next) => {
    const id = req.body.adminId;
    if(!id){
      Admin.find()
        .select('_id email password municipality super_admin')
        .populate('municipality')
        .exec()
        .then(docs => {
            res.status(200).json(docs);
        })
        .catch( err => {
            res.status(500).json({
                error : err
            });
        });
    }
    else{
        Admin.find({_id : id})
        .select('_id email password municipality super_admin')
        .populate('municipality')
        .exec()
        .then( doc => {
            if(doc) {
                res.status(200).json(doc);
            }
            else {
                res.status(404).json({
                   message : "No valid entry found for the given ID" 
                }) 
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        });
    }
};