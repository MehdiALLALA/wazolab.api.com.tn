const mongoose = require("mongoose");
const Municipality = require("../models/municipality");
const validateMunicipalityInput = require('../validation/municipality');

exports.municipalities_get_municipality = (req, res, next) => {
    const id = req.body.municipalityId;
    if(!id){
        Municipality.find()
        .select('_id name city governorate cityLatitude cityLongitude')
        .exec()
        .then(docs => {
            res.status(200).json(docs);
        })
        .catch( err => {
            res.status(500).json({
                error : err
            });
        });
    }
    else{
        Municipality.find({_id : id})
        .select('_id name city governorate cityLatitude cityLongitude')
        .exec()
        .then( doc => {
            if(doc) {
                res.status(200).json(doc);
            }
            else {
                res.status(404).json({
                   message : "No valid entry found for the given ID" 
                }) 
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        });
    }
};

exports.municipalities_create_municipality = (req, res, next) => {


    const { errors, isValid } = validateMunicipalityInput(req.body);
    
    if (!isValid) {
      return res.status(400).json(errors);
    }

    Municipality
    .find({city : req.body.city.toUpperCase() , governorate : req.body.governorate.toUpperCase() , cityLatitude : req.body.cityLatitude , cityLongitude : req.body.cityLongitude})
    .exec()
    .then(rs => {
        if(rs.length > 0){
            errors.governorate = 'Governorate already exists';
            errors.city = 'Municipality already exists';
            errors.cityLatitude = 'Municipality latitude already exists';
            errors.cityLongitude = 'Municipality longitude already exists';
            return res.status(409).json(errors);
        }
        else {
            const municipality = new Municipality({
                _id : new mongoose.Types.ObjectId(),
                city : req.body.city.toUpperCase(),
                governorate : req.body.governorate.toUpperCase(),
                cityLatitude : req.body.cityLatitude, 
                cityLongitude : req.body.cityLongitude
            });
            municipality
            .save()
            .then(result => {
            res.status(201).json({
                message: "Created municipality successfully",
                createdMunicipality: municipality
            });
            })
            .catch(err => {
            res.status(500).json({
                  error: err
                });
            });
        }})
        .catch(err => {
            res.status(500).json({
                error: err
                })
        });
};


exports.municipalities_delete_municipality = (req, res, next) => {
    const id = req.body.municipalityId;

    if(id){
        Municipality.deleteOne({ _id: id})
        .exec()
        .then( result => {
            res.status(200).json({
                message : "municipality deleted sucessfully"
            });
        })
        .catch( err => {
            res.status(500).json({
                error : err
            });
        });
    }
    else{
    res.status(400).json({
        error : "missing municipalityId"
    })
}
};

exports.municipalities_update_municipality = (req, res, next) => {
    const id = req.body.municipalityId;
    const updateOps = {};
    if (id && req.body.Props && req.body.Props.length > 0) {
        for (const ops of req.body.Props) {
            updateOps[ops.propName] = ops.value.toUpperCase();
        }
        const { errors, isValid } = validateMunicipalityInput(updateOps);
    
        if (!isValid) {
          return res.status(400).json(errors);
        }
        Municipality
        .find(updateOps)
        .exec()
        .then(rs => {
            if(rs.length > 0){
                errors.governorate = 'Governorate already exists';
                errors.city = 'Municipality already exists';
                errors.cityLatitude = 'Municipality latitude already exists';
                errors.cityLongitude = 'Municipality longitude already exists';
                return res.status(409).json(errors);
            }
            else {
                Municipality.updateOne({ _id: id }, { $set: updateOps })
                .exec()
                .then(result => {
                        res.status(200).json({
                            message: "Municipality updated sucessfully"
                        });
                })
                .catch(err => {
                        res.status(500).json({
                            error: err
                        })
                    });
                 }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        });
    } 
    else {
        res.status(400).json({
            error: "missing municipalityId and/or Props Array"
        })
    }
};