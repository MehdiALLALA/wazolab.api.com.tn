const mongoose = require('mongoose');

const PostSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    date : { type: String, required: true}, 
    rating: { type: String, required: true},
    post_image: { type: String, required: true},
    altitude: { type: String, required: true},
    longitude: { type: String, required: true},
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    municipality: { type: mongoose.Schema.Types.ObjectId, ref: 'Municipality', required: true }
});

module.exports = mongoose.model('Post', PostSchema);