const mongoose = require('mongoose');

const MunicipalitySchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    city : { type: String, required: true },
    governorate: { type: String, required: true },
    cityLatitude: { type: Number, required: true },
    cityLongitude: { type: Number, required: true }
});

module.exports = mongoose.model('Municipality', MunicipalitySchema);