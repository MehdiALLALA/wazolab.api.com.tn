const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('config');


const PostRoutes = require('./api/routes/posts');
const userRoutes = require('./api/routes/users');
const municipalityRoutes = require('./api/routes/municipalities');
const adminRoutes = require('./api/routes/admins');
const db = config.get('mongoURI');


mongoose.connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.set('useCreateIndex', true);

app.use(morgan('dev'));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === 'OPTIONS') {
        res.header("Access-Control-Allow-Methods", "POST");
        return res.status(200).json({});

    }
    next();
});

app.use('/posts',PostRoutes);
app.use('/users',userRoutes);
app.use('/municipalities',municipalityRoutes);
app.use('/admins',adminRoutes);




app.use((req, res, next) => {
    const error = new Error('Route Not Found');
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;